var five = require("johnny-five"),

  board = new five.Board({port: "/dev/rfcomm0"});
    /*function(){
    if(os.type() == "Windows_NT"){
      return {port: "COM5"};
    } else {
      return {port: "/dev/ttyS99"};
    }
  });depende del puerto asignado al bluetooth, ver como automatizar
*/

var led;
var estadoBoard = false;
var variacion = 15;
var velocidadGiro = 70;
var velocidad = 0;
var sentido = 'w';
var orden = 'z';
var giro = 1;

board.on("ready", function() {
  console.log("Inicializando placa");
  motorios = new five.Motors([
    { pins: { dir: 6, cdir: 5, pwm: 11}, invertPWM: false },
    { pins: { dir: 4, cdir: 3, pwm: 10}, invertPWM: false }
  ]);
  led = new five.Led(13);
  led.off();
  motorios.brake();//que los motores inicien detenidos
  console.log("Placa lista");
  estadoBoard = true;
});

//controles de vehiculo
exports.controls = function(data){
  if(estadoBoard){
    switch(data) {
      case 'a': //a dobla a la izquierda
        motorios[1].reverse(velocidadGiro/giro);
        motorios[0].forward(velocidadGiro);
        sentido = 'a';
        orden = 'a';
      break;
      case 'd': //d dobla a la derecha
        motorios[1].forward(velocidadGiro);
        motorios[0].reverse(velocidadGiro/giro);
        sentido = 'd';
        orden = 'd';
      break;
      case 'w': //w avanza
        motorios[0].forward(velocidad);
        motorios[1].forward(velocidad);
        sentido = 'w';
        orden = 'w';
      break;
      case 's': //s retrocede
        motorios[0].reverse(velocidad);
        motorios[1].reverse(velocidad);
        sentido = 's';
        orden = 's';
      break;
      case 'e': //e disminuye la velocidad si es posible
        if(velocidad >= variacion) {
            velocidad = velocidad - variacion;
        }
        if(sentido == 'w' || sentido == 's'){
          this.controls(sentido);
        }
        orden = 'e';
      break;
      case 'q': //q aumenta la velocidad si es posible
        if(velocidad <= 255-variacion) {
            velocidad = velocidad + variacion;
        }
        if(sentido == 'w' || sentido == 's'){
          this.controls(sentido);
        }
        orden = 'q';
      break;
      case 'z': //z frena
        motorios.brake();
        sentido = 'z';
        orden = 'z';
      break;
      //led de prueba
      case 'l':
        led.on();
      break;
      case 'k':
        led.off();
      break;
    }
  }
  //console.log("orden: " + data + " velocidad: " + velocidad);
};

exports.datos = function() {
  datos = {velocidad: velocidad, orden: orden};
  return datos;
};
