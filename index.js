

//Constantes
var PORT = 3000;
var idActual = "none";

//Inicializar express
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io')(server);
//inicializo la placa vacia
var board = "";

app.get('/', function (req, res) {
  //res.append('ip', 'localhost');
  res.sendFile(process.cwd() + '/public/index.html');
});

//activar escucha de servidor web
server.listen(PORT, function(){
  console.log('listening on port: ' + PORT);
});

io.on('connection', function (socket) {
  console.log('New connection from ' + socket.request.connection.remoteAddress);
  console.log('client ID: ' + socket.id);
  
  //iniciar placa
  socket.on('bordear', function() {
    if(board == ""){
      console.log('Iniciando la placa');
      board = require("./board/arduino.js");
    }
  });
  //tomar control
  socket.on('control', function() {
    if(idActual == "none"){
        console.log('control pedido por: ' + socket.id);
        idActual = socket.id;
        io.emit('mando', idActual);
    } else {
      if(idActual == socket.id) {
        idActual = "none";
        io.emit('mando', idActual);
      }
    }
  });

  //enviar comando
  socket.on('pressed', function(data) {
    //console.log('tecla apretada ' + data.paquete);
    if(idActual == socket.id){
      board.controls(data.paquete);
      io.emit('actualizar', board.datos());
    }
  });

  //en desconexion de usuario, control de mando
  socket.on('disconnect', function() {
    console.log('desconexion de: ' + socket.id);
    if(idActual == socket.id) {
      idActual = "none";
      io.emit('mando', idActual);
    }
  });

  console.log('Id actual: ' + idActual);

  /*if(board != ""){
    board.on("exit", function() {
      board = "";
    });
  };*/

});
